# EliteQuant_Excel
Excel量化模型和交易平台

* [平台介绍](#平台介绍)
* [项目纲要](#项目纲要)
* [参与开发](#参与开发)
* [项目安装](#项目安装)
* [开发环境](#开发环境)
* [演示案例](#演示案例)
* [开发计划](#开发计划)

---

## 平台介绍

EliteQuant 是一个开源并永久免费的统一量化交易平台，由量化投资者所写并为量化投资者服务。它同时在[github](https://github.com/EliteQuant) 和 [码云](https://gitee.com/EliteQuant)上开源。

统一这个词有两层意思
- 首先是统一的回测和实盘交易。只需将数据源在回测和实盘间切换即可，最大限度保持策略稳定性和真实性
- 其次，多语言编写的平台在交易结构和绩效评估上是一致的。所以在与其他交易员就策略，想法和绩效方面进行复制和交流就变得非常容易和方便。

EliteQuant 相关项目包括
- [量化投资交易资源汇总](https://gitee.com/EliteQuant/EliteQuant)
- [C++](https://gitee.com/EliteQuant/EliteQuant_Cpp)
- [Python](https://gitee.com/EliteQuant/EliteQuant_Python)
- [Matlab](https://gitee.com/EliteQuant/EliteQuant_Matlab)
- [R]()
- [C#]()
- [Excel](https://gitee.com/EliteQuant/EliteQuant_Excel)
- [Java]()
- [Scala]()
- [Go]()
- [Julia]()

## 项目纲要

EliteQuant Excel是用于定价，投资组合和风险管理的Excel插件工具。它使用QuantLib作为利率产品，CDS，股票和商品的定价引擎。蒙特卡洛引擎将QuantLib扩展到PFE和CVA等应用计算上。

有关更多详情，请查看[介绍性博客文章](http://www.elitequant.com/cn/2017/10/22/elitequant-excel-one/) 和 [优酷视频](http://v.youku.com/v_show/id_XMzE4MTg1ODY2NA==.html?spm=a2hzp.8244740.0.0)。

## 参与开发

我们欢迎任何形式的贡献，包括发现问题，发送代码块，或创建拉请求。通过共享代码架构，这还会帮助使用其他语言的交易者。

## 项目安装

不需要安装，直接下载Compiled.zip 文件并解压使用。

####运行编译好的可执行文件

下载位于项目根目录下的文件Compiled.zip。启动Excel并从解压文件的位置打开EliteQuantExcel-addin-x86.xll或EliteQuantExcel-addin-x64.dll。一个名为EliteQuant的功能区选项卡应该显示出来。所有函数以eq开头，例如，调用Excel函数eqtimetoday（）它将返回今天的日期。在演示下拉菜单中有Black-Scholes和市场历史数据工作簿。

####运行源代码

（1）将boost，编译的QuantLib和swigwin放在文件夹 D:\workspace 中。如果使用不同的路径，则必须相应地更改项目引用路径。

（2）下载并编译项目。

（3）从Excel中打开xll文件，功能区将显示出来。

## 开发环境

以下是我们正在使用的环境
* boost c ++库v1.6.5
* QuantLib c ++库v1.11
* Swig Windows 3.0.12
* Microsoft Visual Studio 社区免费版 2017
* Microsoft .Net框架4.7（您可能需要将其降级以符合您的机器要求）
* Microsoft Office Excel Professional Plus 2016

## 演示案例

Black-Scholes 模型

![Black Scholes](/resource/black_scholes.gif?raw=true "Black Scholes")

历史数据

![历史数据](/resource/hist_data.gif?raw=true "历史数据")

## 开发计划